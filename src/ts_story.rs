use crossterm::{event, execute, style::{SetForegroundColor, Print}};
use crossterm::cursor::{MoveTo, MoveLeft};
use crate::{get_cleared_screen, TS_VERSION};
use crate::ts_options::{get_color, TSColors};


pub(crate) fn story() {
    let mut stdout = get_cleared_screen();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));

    execute!(stdout, reset_fg_color,
MoveTo(2, 3), Print("Typespeed ".to_owned() + TS_VERSION),
MoveTo(2, 5), Print("Typespeed is made by  Jani Ollikainen  <bestis@iki.fi>"),
MoveTo(2, 6), Print("                    & Jaakko Manelius  <jman@iki.fi>"),
MoveTo(2, 7), Print("Current maintainer is Tobias Stoeckmann  <tobias@bugol.de>"),
MoveTo(2, 9), Print("Typespeed's idea is ripped from ztspeed (a dos game made"),
MoveTo(2, 10), Print("by Zorlim). Typespeed doesn't give scores that can be"),
MoveTo(2, 11), Print("compared with ztspeed anymore. We wanted to use our way."),
MoveTo(2, 13), Print("Idea of the game should be clear to anyone, just type and"),
MoveTo(2, 14), Print("type it fast, or be a lewser."),
MoveTo(2, 16), Print("Bugs/Ideas/Comments to <tobias@bugol.de>"),
        SetForegroundColor(get_color(TSColors::Auxiliary)),
MoveTo(2, 18), Print("Press any key to continue..."),
        SetForegroundColor(get_color(TSColors::Cursor)),
        Print(" "), MoveLeft(1),
).unwrap();

    event::read().unwrap();
}