use std::fmt::Debug;
use std::io::{stdout, Write, Stdout};
use std::ops::Range;
use std::str::FromStr;
use crossterm::{event, execute, queue, terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen}};
use crossterm::event::{Event, KeyCode};
use crossterm::style::{Color, SetBackgroundColor};
use crossterm::terminal::{Clear, size, ClearType::All};
use crossterm::style::{SetForegroundColor, Print, Stylize};
use crossterm::cursor::{MoveTo, Show};
use crossterm::terminal::ClearType::FromCursorUp;

use crate::{INPUT_MAX, OPTIONS};

use crate::ts_options::{get_color};
use crate::ts_options::TSColors;
use crate::ts_stats::{stats_legends, stats_metrics};
use libc::{ISIG, TCSANOW, termios};
use std::os::raw::c_int;

extern {
    fn tcgetattr(fd: c_int, termios: *mut termios) -> c_int;
    fn tcsetattr(fd: c_int, optional_actions: c_int, termios: *const termios) -> c_int;
}

// crossterm's alternate mode deactivates unix signals.
// By settings ISIG, signals will be created again: https://linux.die.net/man/3/tcgetattr
// crossterm will automatically reset to old settings, so we don't have to store anything here.
fn activate_signals() {
    let mut ios: termios = termios { c_iflag: 0, c_oflag: 0, c_cflag: 0, c_lflag: 0, c_line: 0, c_cc: [0; 32], c_ispeed: 0, c_ospeed: 0 };

    //load current configuration into ios
    unsafe { tcgetattr(libc::STDIN_FILENO, &mut ios) };

    //set flag to activate signals again
    ios.c_lflag = ios.c_lflag | ISIG;

    //tcsetattr returns 0 on success
    let tc = unsafe { tcsetattr(libc::STDIN_FILENO, TCSANOW, &mut ios) };

    assert_eq!(tc, 0);
}

pub(crate) fn create_terminal() -> Result<(), &'static str> {
    let s = size().unwrap();
    if s.0 < 80 || s.1 < 24 {
        return Err("typespeed: You need at least 80 x 24 terminal!");
    };

    enable_raw_mode().unwrap();
    execute!(stdout(), EnterAlternateScreen, SetBackgroundColor( Color::Black), Clear(All)).unwrap();

    activate_signals();
    Ok(())
}

pub(crate) fn shut_down() {
    destroy_terminal();
    std::process::exit(0);
}

pub(crate) fn destroy_terminal() {
    disable_raw_mode().unwrap();

    execute!(
        stdout(),
        LeaveAlternateScreen
    ).unwrap();
}

pub(crate) fn get_number<T: FromStr>(prompt: &str, max_length: usize, pos: (u16, u16), rn: Range<T>) -> Option<T> where <T as FromStr>::Err: Debug, T: PartialOrd
{
    let tmp = match get_input(prompt.to_string(), max_length, pos).parse::<T>() {
        Ok(x) => x,
        _ => return None,
    };

    return match rn.contains(&tmp) {
        true => Some(tmp),
        false => None,
    };
}


pub(crate) fn get_input(prompt: String, max_length: usize, pos: (u16, u16)) -> String {
    let prompt_length: u16 = prompt.chars().count() as u16;
    let cursor_offset: u16 = pos.0 + prompt_length;

    let mut stdout = stdout();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));
    let mut input_string = String::from("");

    queue!(stdout, reset_fg_color,
            MoveTo(pos.0, pos.1), Print(prompt),
        ).unwrap();

    loop {
        execute!(stdout, reset_fg_color,
            MoveTo(cursor_offset, pos.1), Print(format!("{:<INPUT_MAX$}", input_string)),
            MoveTo(cursor_offset + input_string.chars().count() as u16, pos.1),
            Show,
        ).unwrap();

        loop { // redraw only in case input_string changes
            if let Event::Key(key) = event::read().unwrap() {
                match key.code {
                    KeyCode::Enter => return input_string,
                    KeyCode::Backspace => {
                        drop(input_string.pop());
                        break;
                    }
                    KeyCode::Char(c) if input_string.len() < max_length => {
                        input_string.push(c);
                        break;
                    }
                    _ => (),
                }
            }
        }
    }
}


pub(crate) fn get_cleared_screen() -> Stdout {
    let mut stdout = stdout();
    execute!(stdout, MoveTo(79, 21), SetForegroundColor( get_color(TSColors::Cursor) ), SetBackgroundColor(Color::Black), Clear(FromCursorUp)).unwrap();

    stdout
}

pub(crate) fn footer() {
    let mut stdout = stdout();

    queue!(stdout, MoveTo(0, 22), Print("────────────────────────────────────────────────────────────────────────────────".with(get_color(TSColors::Auxiliary)))).unwrap();

    // write cheat mode indicator CHE
    if OPTIONS.read().unwrap().use_cheat_mode {
        queue!(stdout, MoveTo(70, 22), Print("CHE")).unwrap();
    }

    if OPTIONS.read().unwrap().multiplayer {
        queue!(stdout, MoveTo(70, 22), SetForegroundColor( get_color(TSColors::Cursor)), Print("H2H"), ).unwrap();
    }

    stats_legends();
    stats_metrics(&Default::default());

    stdout.flush().unwrap();
}