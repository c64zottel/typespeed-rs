use crossterm::style::Color;
use crate::ts_options::{get_color, TSColors};

pub struct TsWordOnScreen {
    pub(crate) pos: (u16, u16),
    pub(crate) length: u16,
    pub(crate) word: String,
    pub(crate) color: Color,
}

impl TsWordOnScreen {
    pub(crate) fn advance_position(&mut self) {
        // advance one char
        self.pos.0 += 1;

        // color words
        match self.pos.0 {
            50 => self.color = get_color(TSColors::FloatingHot),
            65 => self.color = get_color(TSColors::FloatingCrit),
            _ => (),
        }
    }
}

impl Default for TsWordOnScreen {
    fn default() -> Self {
        Self {
            pos: (0, 0),
            length: 0,
            word: "".to_string(),
            color: get_color(TSColors::FloatingNormal),
        }
    }
}