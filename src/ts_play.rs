use std::io::Write;
use std::time::Duration;
use crossterm::{event, execute, queue};
use crossterm::event::{Event, KeyCode, poll};
use crossterm::style::{SetForegroundColor, Print, Color};
use crossterm::cursor::{Show, Hide, MoveTo};
use crate::ts_game_variables::TsGameVariables;
use crate::{footer, get_cleared_screen};
use crate::ts_options::get_color;
use crate::ts_options::TSColors::Cursor;
use crate::ts_stats::stats_metrics;
use crate::INPUT_MAX;
use crate::ts_connection::TsConnection;
use crate::ts_game_over::game_over;
use crate::ts_network::wait_for_party;


pub(crate) fn play(ts_connection: Option<TsConnection>) {
    let mut input_string = String::from("");
    let wordlist = crate::ts_file::get_wordlist();
    let game_vars = &mut TsGameVariables::new(ts_connection, wordlist);
    wait_for_party(&mut game_vars.word_manager.ts_connection);

    get_cleared_screen();
    footer();

    update_input_line(&input_string); // preset cursor color
    loop {
        // user input
        execute!(std::io::stdout(), MoveTo( 2 + input_string.chars().count() as u16, 23), Show).unwrap();
        while game_vars.tick_not_over() {
            match get_input(&mut input_string, 19) {
                InputType::Updated => update_input_line(&input_string),
                InputType::NoChange => {}
                InputType::Enter(entered_word) => {
                    update_input_line(&input_string);
                    if let Some(x) = game_vars.remove_potential_hit(&entered_word) {
                        clear_space(x.0, x.1);
                    }
                }
            }
        }
        execute!(std::io::stdout(), Hide).unwrap();

        game_vars.update_time();

        for i in game_vars.advance_words().iter() {
            clear_space(i.0, i.1);
        }

        game_vars.update_round();

        stats_metrics(&game_vars.stats);

        for word in game_vars.into_iter() {
            place_text(&word.word, word.color, word.length as usize, word.pos);
        }
        std::io::stdout().flush().unwrap();

        if game_vars.is_game_over() {
            game_over(&game_vars.stats);
            // TODO add highscore screen
            return;
        }
    }
}


fn clear_space(pos: (u16, u16), length: usize) {
    execute!( std::io::stdout(), Hide,
            MoveTo( pos.0, pos.1 ),
            Print(format!("{: >length$}", " "))
    ).unwrap();
}

fn place_text(text: &String, color: Color, mut length: usize, pos: (u16, u16)) {
    let mut e = 0;
    if pos.0 > 0 { // clean the position in front of the word when pos > 0
        e = 1;
        length += 1; // used in format!, which can not handle a struct field
    }
    queue!( std::io::stdout(),
                MoveTo(pos.0 - e, pos.1),
                SetForegroundColor(color),
                Print(format!("{: >length$}", text)),
    ).unwrap();
}


fn update_input_line(input_string: &String) {
    execute!( std::io::stdout(), SetForegroundColor(get_color(Cursor)),
        MoveTo(2, 23),
        Print(format!("{: <INPUT_MAX$}", input_string)),
        //Since we used format above, we need to reposition the cursor
        MoveTo( 2 + input_string.chars().count() as u16, 23),
    ).unwrap();
}

enum InputType {
    NoChange,
    Enter(String),
    Updated,
}

// Returns true if input_string has changed, otherwise false.
fn get_input(input_string: &mut String, max_length: usize) -> InputType {
    if poll(Duration::from_millis(100)).unwrap() {
        if let Event::Key(key) = event::read().unwrap() {
            match key.code {
                KeyCode::Enter | KeyCode::Char(' ') if !input_string.is_empty() => {
                    let s = input_string.clone();
                    input_string.clear();
                    return InputType::Enter(s);
                }
                KeyCode::Backspace => drop(input_string.pop()),
                KeyCode::Char(c) if input_string.len() < max_length => input_string.push(c),
                _ => return InputType::NoChange,
            }
            return InputType::Updated;
        }
    }
    InputType::NoChange
}