use std::io::{BufRead, BufReader, BufWriter, Error, Write};
use std::net::TcpStream;

pub(crate) struct TsConnection {
    stream: TcpStream,
    reader: BufReader<TcpStream>,
    writer: BufWriter<TcpStream>,
}

const GAME_READY: &str = " GAME START\n";

impl TsConnection {
    pub(crate) fn new(stream: TcpStream) -> TsConnection {
        Self {
            reader: BufReader::new(stream.try_clone().unwrap()),
            writer: BufWriter::new(stream.try_clone().unwrap()),
            stream,
        }
    }

    pub(crate) fn write(&mut self, text: &String) {
        self.writer.write(text.as_ref()).unwrap();
        self.writer.flush().unwrap();
    }

    pub(crate) fn read(&mut self) -> Result<String, Error> {
        let text = &mut String::new();

        match self.reader.read_line(text) {
            Ok(_) => Ok(text.to_owned()),
            Err(x) => Err(x),
        }
    }

    pub(crate) fn set_nonblocking(&mut self) {
        self.stream.set_nonblocking(true).unwrap();
    }

    pub(crate) fn set_blocking(&mut self) {
        self.stream.set_nonblocking(false).unwrap();
    }

    pub(crate) fn wait_opponent_ready(&mut self) {
        // let the other side know we are ready
        self.write(&GAME_READY.to_string());

        self.set_blocking();

        // wait until opponent is ready
        self.read().unwrap();
        // we expect here GAME_READY
        self.set_nonblocking();
    }
}