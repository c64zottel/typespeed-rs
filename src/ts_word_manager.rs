use crate::{MAX_LINES, OPTIONS, RULES};
use crate::ts_connection::TsConnection;
use crate::ts_word_on_screen::TsWordOnScreen;

pub struct TsWordManager {
    pub(crate) words_on_screen: [Box<TsWordOnScreen>; 22],
    pub(crate) ts_connection: Option<TsConnection>,
    words: Vec<String>,
    free_slots_of_moving_words: Vec<usize>,
    chars_on_screen: u16,
}

impl TsWordManager {
    pub(crate) fn remove_potential_hit(&mut self, entered_word: &String) -> Option<((u16, u16), usize)> {
        for index in 0..self.words_on_screen.len() {
            let word = &mut self.words_on_screen[index];
            if word.word == *entered_word {
                let word_metrics = Some((word.pos, word.length as usize));
                self.remove_word(index);
                if OPTIONS.read().unwrap().multiplayer {
                    // TS original expects a newline at the end of the word
                    self.ts_connection.as_mut().unwrap().write(&format!("{}\n", entered_word));
                }
                return word_metrics;
            }
        }
        None
    }

    pub(crate) fn add_network_word(&mut self) {
        if let Some(ts_connection) = self.ts_connection.as_mut() {
            if let Ok(mut text) = ts_connection.read() {
                text = text.trim().to_string();
                eprintln!("{:?}", text);
                if self.is_not_duplicate(&text) {
                    let y = self.get_random_slot_position();
                    let word = self.words_on_screen[y].as_mut();
                    word.pos = (0, y as u16);
                    word.length = text.chars().count() as u16;
                    word.word = text;

                    self.chars_on_screen += word.length as u16;
                }
            }
        }
    }

    pub(crate) fn new(ts_connection: Option<TsConnection>, words: Vec<String>) -> Self {
        Self {
            words,
            ts_connection,
            chars_on_screen: 0,
            words_on_screen: [(); MAX_LINES].map(|_| Box::new(TsWordOnScreen::default())),
            free_slots_of_moving_words: (0..MAX_LINES).map(|x| x).collect(),
        }
    }

    pub(crate) fn add_word(&mut self, score: u16) {
        if self.enough_words_on_screen(score) { return; }

        let new_word = self.random_non_duplicate_word();
        let y = self.get_random_slot_position();
        let word = self.words_on_screen[y].as_mut();
        word.pos = (0, y as u16);
        word.length = new_word.chars().count() as u16;
        word.word = new_word;

        self.chars_on_screen += word.length as u16;
    }

    pub(crate) fn remove_word(&mut self, word_index: usize) {
        let word = &mut self.words_on_screen[word_index];
        self.free_slots_of_moving_words.push(word.pos.1 as usize);
        self.chars_on_screen -= word.length;

        *word = Box::new(TsWordOnScreen::default());
    }

    fn enough_words_on_screen(&self, score: u16) -> bool {
        // from the original version:
        // length = number of chars on screen, wc = words on screen
        //   if (length < now.score / 4 + 1 || wc < rules.minwords) addword()
        // addword() checks if we have reached the max number of words on screen

        // no free slots?
        if self.free_slots_of_moving_words.len() == 0 {
            return true;
        }

        let rules = RULES.read().unwrap();
        let words_on_screen = MAX_LINES - self.free_slots_of_moving_words.len();

        if words_on_screen < rules.min_max_words.start {
            return false;
        }

        if words_on_screen == rules.min_max_words.end {
            return true;
        }

        if self.chars_on_screen < (score / 4 + 1) {
            return false;
        }

        true
    }

    fn get_random_slot_position(&mut self) -> usize {
        let index = fastrand::usize(0..self.free_slots_of_moving_words.len());

        self.free_slots_of_moving_words.swap_remove(index)
    }

    fn random_non_duplicate_word(&self) -> String {
        loop {
            // Range starts at 1 because the first line is a comment exp.: #Unix commands
            let new_word = &self.words[fastrand::usize(1..self.words.len())];
            if self.is_not_duplicate(new_word)
            { return new_word.to_owned(); }
        }
    }

    fn is_not_duplicate(&self, new_word: &String) -> bool {
        !self.words_on_screen.iter()
            .any(|w| w.word.eq(new_word))
    }
}

