use crossterm::{event, execute};
use crossterm::event::{Event, KeyCode};
use crossterm::cursor::{Show, MoveTo, MoveLeft};
use crossterm::style::{SetForegroundColor, Print};
use crate::ts_options::{get_color, TSColors};
use crate::{footer, get_cleared_screen, TS_VERSION};
use crate::ts_play::play;


pub(crate) fn start() {
    footer();
    loop {
        let mut stdout = get_cleared_screen();

        let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));

        execute!(stdout, reset_fg_color,
        MoveTo(30, 3), Print("Typespeed ".to_owned() + TS_VERSION),
        MoveTo(30, 5), Print("1. Test Your Speed"),
        MoveTo(30, 6), Print("2. Network Head2Head"),
        MoveTo(30, 7), Print("3. Story/Credits/RTFM!"),
        MoveTo(30, 8), Print("4. Show Highscores"),
        MoveTo(30, 9), Print("5. Options"),
        MoveTo(30, 10), Print("6. Game Rules"),
        MoveTo(30, 11), Print("7. Quit"),
        MoveTo(30, 13), Print("Choose:"),
            SetForegroundColor(get_color(TSColors::Cursor)),
            Print("  "), MoveLeft(1), Show
        ).unwrap();


        if let Event::Key(key) = event::read().unwrap() {
            match key.code {
                KeyCode::Char('1') => play(None),
                KeyCode::Char('2') => match crate::ts_network::network() {
                    None => {} // no connection? -> no game
                    connection => play(connection),
                },
                KeyCode::Char('3') => crate::ts_story::story(),
                KeyCode::Char('5') => crate::ts_options::options(),
                KeyCode::Char('6') => crate::ts_rules::rules(),
                KeyCode::Char('7') => return (),
                _ => (),
            }
        }
    }
}

