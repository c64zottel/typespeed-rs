use std::ops::Range;
use crossterm::{event, ExecutableCommand, execute};
use crossterm::cursor::{MoveTo, MoveLeft, Hide, Show};
use crossterm::event::{Event, KeyCode};
use crossterm::style::{SetForegroundColor, Print};

use crate::ts_options::{TSColors, get_color};
use crate::{get_cleared_screen, get_number, MAX_LINES, RULES, TS_VERSION};

pub(crate) struct TsRules {
    pub(crate) misses: u8,
    pub(crate) min_max_words: Range<usize>,
    pub(crate) min_speed: u16,
    pub(crate) max_speed: u16,
    pub(crate) step: u16,
    min_len: u8,
    max_len: u8,
    highscore_allowed: bool,
    smooth: bool,
}

impl Default for TsRules {
    fn default() -> Self {
        // These are the default rules
        // atm hardcoded, equal to rules/template
        Self {
            misses: 10,
            min_max_words: Range { start: 1, end: MAX_LINES + 1 },
            min_speed: 3,
            max_speed: 0,
            step: 175,
            min_len: 1,
            max_len: 19,
            highscore_allowed: true,
            smooth: true,
        }
    }
}


pub(crate) fn rules() {
    loop {
        loop {
            rules_template();
            if let Event::Key(key) = event::read().unwrap() {
                std::io::stdout().execute(Hide).unwrap(); // prevent cursor flickering all over the screen.
                match key.code {
                    KeyCode::Char('1') => return,
                    KeyCode::Char('2') => if let Some(x) = get_number::<u8>("Misses Limit: ", 10, (30, 17), Range { start: 1, end: 100 }) {
                        RULES.write().unwrap().misses = x;
                        break;
                    }
                    KeyCode::Char('3') => {
                        let mut rules = RULES.write().unwrap();
                        if let Some(x) = get_number::<u8>("Min Length: ", 10, (30, 17), Range { start: 1, end: 20 }) {
                            rules.min_len = x;
                        }
                        if let Some(x) = get_number::<u8>("Max Length: ", 10, (30, 17), Range { start: 1, end: 20 }) {
                            rules.max_len = x;
                        }
                        if rules.min_len > rules.max_len { rules.max_len = rules.min_len; }
                        break;
                    }
                    KeyCode::Char('4') => {
                        let mut rules = RULES.write().unwrap();
                        if let Some(x) = get_number::<usize>("Min Words: ", 10, (30, 17), Range { start: 1, end: 23 }) {
                            rules.min_max_words.start = x
                        }
                        if let Some(x) = get_number::<usize>("Max Words: ", 10, (30, 17), Range { start: 1, end: 23 }) {
                            rules.min_max_words.end = x;
                        }
                        if rules.min_max_words.start > rules.min_max_words.end { rules.min_max_words.end = rules.min_max_words.start; }
                        break;
                    }
                    KeyCode::Char('6') => if let Some(x) = get_number("Speed Step: ", 10, (30, 17), Range { start: 1, end: 1000 }) {
                        let mut rules = RULES.write().unwrap();
                        rules.step = x;
                        break;
                    }
                    KeyCode::Char('7') => {
                        let mut rules = RULES.write().unwrap();
                        if let Some(x) = get_number::<u16>("Min Speed: ", 10, (30, 17), Range { start: 1, end: 100 }) {
                            rules.min_speed = x
                        }
                        if let Some(x) = get_number::<u16>("Max Speed: ", 10, (30, 17), Range { start: 1, end: 100 }) {
                            rules.max_speed = x;
                        }
                        if rules.min_speed > rules.max_speed { rules.max_speed = rules.min_speed; }
                        break;
                    }
                    KeyCode::Char('8') => {
                        let mut rules = RULES.write().unwrap();
                        rules.smooth = !rules.smooth;
                        break;
                    }
                    KeyCode::Char('9') => return,
                    _ => drop(std::io::stdout().execute(Show).unwrap()),
                }
            }
        }
        std::io::stdout().execute(Show).unwrap();
    }
}

fn rules_template() {
    let mut stdout = get_cleared_screen();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));
    let metric_fg_color = SetForegroundColor(get_color(TSColors::Metric));
    let rules = RULES.read().unwrap();

    execute!(stdout, reset_fg_color,
MoveTo(30, 3), Print("Typespeed ".to_owned() + TS_VERSION),
MoveTo(30, 5), Print("Current Rule Set: "),
MoveTo(30, 7), Print("1. Load Another Rule Set"),
MoveTo(30, 8), Print("2. Misses Limit: "),
            metric_fg_color,
            Print(format!("{}", rules.misses)),
            reset_fg_color,
MoveTo(30, 9), Print("3. Word Length: "),
            metric_fg_color,
            Print(format!("{: >2}   {: >2}", rules.min_len, rules.max_len)),
            reset_fg_color, MoveLeft(4),
            Print("-"),
MoveTo(30, 10), Print("4. Words On Screen: "),
            metric_fg_color,
            Print(format!("{: >2}   {: >2}", rules.min_max_words.start, rules.min_max_words.end)),
            reset_fg_color, MoveLeft(4),
            Print("-"),
MoveTo(30, 11), Print("   High Score Enabled: "),
            metric_fg_color,
            Print( match rules.highscore_allowed {
            true => "yes",
            false => "no", }),
            reset_fg_color,
MoveTo(30, 12), Print("6. Speed Step: "),
            metric_fg_color,
            Print(format!("{: >3}", rules.step)),
            reset_fg_color,
MoveTo(30, 13), Print("7. Speed Range: "),
            metric_fg_color,
            Print(format!("{: >2}   {: >2}", rules.min_speed, rules.max_speed)),
            Print(match rules.max_speed {
                0 => " (unlimited)",
                _ => "",
            }),
            reset_fg_color, MoveLeft(16),
            Print("-"),
MoveTo(30, 14), Print("8. Smoothness: "),
            metric_fg_color,
            Print( match rules.smooth {
            true => "yes",
            false => "no", }),
            reset_fg_color,
MoveTo(30, 15), Print("9. Return"),
MoveTo(30, 17), Print("Choose:"),
            SetForegroundColor(get_color(TSColors::Cursor)),
            Print("  "), MoveLeft(1),
).unwrap();
}