use crossterm::{event, execute, style::{SetForegroundColor, Print}};
use crossterm::cursor::{MoveTo, MoveLeft};
use crate::{get_cleared_screen, TS_VERSION};
use crate::ts_options::{get_color, TSColors};
use crate::ts_stats::TsStats;


pub(crate) fn game_over(stats: &TsStats) {
    let mut stdout = get_cleared_screen();
    let fg_color_rank = SetForegroundColor(get_color(TSColors::Rank));
    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));

    execute!(stdout, reset_fg_color,
MoveTo(20, 3), Print("Typespeed ".to_owned() + TS_VERSION),
//empty line
MoveTo(20, 5), Print("You achieved:"),
//empty line
MoveTo(20, 7), Print("Rank:"),
MoveTo(20, 8), Print("Score:"),
MoveTo(20, 9), Print("WPM:"),
MoveTo(20, 10), Print("CPS:"),
MoveTo(20, 11), Print("Typo ratio:"),
MoveTo(20, 12), Print("Typorank:"),
        SetForegroundColor(get_color(TSColors::Auxiliary)),
MoveTo(20, 15), Print("Press any key to continue..."),
        SetForegroundColor(get_color(TSColors::Cursor)),
        Print(" "), MoveLeft(1),
).unwrap();

    execute!(stdout,
        fg_color_rank,
        MoveTo(40, 7), Print(format!("{:<9}", stats.rank)),
        MoveTo(40, 8), Print(format!("{:>4}", stats.score)),
        MoveTo(40, 9), Print(format!("{:>3}", stats.wpm)),
        MoveTo(40, 10), Print(format!("{:0>2.2}", stats.speed)),
    ).unwrap();

    event::read().unwrap(); //TODO: activate blinking cursor, fix stats
}