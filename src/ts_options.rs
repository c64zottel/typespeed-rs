#[derive(Debug, Copy, Clone)]
#[allow(dead_code)] // for Debug and Clone
pub enum TSColors {
    CheatModeOn,
    CheatModeOff,
    // ColorMode defines the color for "ON"
    // in the options menu for "Colors"
    ColorMode,
    DefaultText,
    Cursor,
    Legend,
    Metric,
    Rank,
    Auxiliary,
    FloatingNormal,
    FloatingHot,
    FloatingCrit,
}

pub struct TsOptions {
    pub use_colors: bool,
    pub use_cheat_mode: bool,
    pub multiplayer: bool,
}

use crossterm::{event, execute};
use crossterm::event::{Event, KeyCode};
use crossterm::style::{SetForegroundColor, StyledContent, Stylize, Print, Color::*};
use crossterm::cursor::{MoveTo, MoveLeft};
use TSColors::*;
use crate::{get_cleared_screen, OPTIONS, TS_VERSION};

impl TsOptions {
    pub fn get_color_mode_text(&self) -> StyledContent<&str> {
        match self.use_colors {
            true => "ON".with(get_color(ColorMode)),
            false => "OFF".with(get_color(ColorMode)),
        }
    }

    pub fn get_cheat_mode_text(&self) -> StyledContent<&str> {
        match self.use_cheat_mode {
            true => "ON".with(get_color(CheatModeOn)),
            false => "OFF".with(get_color(CheatModeOff)),
        }
    }
}

pub fn get_color(ts_color: TSColors) -> crossterm::style::Color {
    let use_colors = OPTIONS.read().unwrap().use_colors;
    match use_colors {
        true => {
            match ts_color {
                ColorMode => DarkRed,
                DefaultText => DarkCyan,
                CheatModeOn => DarkRed,
                CheatModeOff => DarkBlue,
                Cursor => Rgb { r: 208, g: 207, b: 204 },
                Legend => DarkCyan,
                Metric => DarkRed,
                Rank => DarkBlue,
                Auxiliary => DarkMagenta,
                FloatingNormal => DarkGreen,
                FloatingHot => Rgb { r: 160, g: 115, b: 76 },
                FloatingCrit => DarkRed,
            }
        }
        false => White,
    }
}

impl Default for TsOptions {
    fn default() -> Self {
        Self {
            use_colors: true,
            use_cheat_mode: false,
            multiplayer: false,
        }
    }
}

impl TsOptions {
    fn toggle_color(&mut self) {
        self.use_colors = !self.use_colors;
    }

    fn toggle_cheat_mode(&mut self) {
        self.use_cheat_mode = !self.use_cheat_mode;
    }
}

pub fn options() {
    loop {
        options_screen();

        if let Event::Key(key) = event::read().unwrap() {
            match key.code {
                KeyCode::Char('1') => {
                    OPTIONS.write().unwrap().toggle_color();
                    crate::footer();
                }
                KeyCode::Char('2') => OPTIONS.write().unwrap().toggle_cheat_mode(),
                KeyCode::Char('3') => break,
                _ => (),
            }
        }
    }
}

pub fn options_screen() {
    let mut stdout = get_cleared_screen();

    let reset_fg_color = SetForegroundColor(get_color(DefaultText));
    let ts_options = OPTIONS.read().unwrap();

    execute!(stdout, reset_fg_color,
MoveTo(30, 3), Print("Typespeed ".to_owned() + TS_VERSION),
MoveTo(30, 5), Print("1. Colors     "),
        Print(ts_options.get_color_mode_text()), reset_fg_color,
MoveTo(30, 6), Print("2. Cheat      "),
        Print(ts_options.get_cheat_mode_text()), reset_fg_color,
MoveTo(30, 7), Print("3. Return"),
MoveTo(30, 11), Print("Choose:"),
        SetForegroundColor(get_color(TSColors::Cursor)),
        Print("  "), MoveLeft(1),
).unwrap();
}