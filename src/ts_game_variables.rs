use std::time::SystemTime;
use crate::RULES;
use crate::ts_connection::TsConnection;
use crate::ts_stats::TsStats;
use crate::ts_word_manager::TsWordManager;

pub struct TsGameVariables {
    pub(crate) stats: TsStats,
    pub(crate) word_manager: TsWordManager,
    // Holds indices on words_on_screen, identifying which slots are still free
    rate: f64,
    start_time: SystemTime,
    tick_time: SystemTime, // Timer to advance chars
}

impl<'a> IntoIterator for &'a TsGameVariables {
    type Item = &'a crate::ts_word_on_screen::TsWordOnScreen;
    type IntoIter = TsGameVariablesIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        TsGameVariablesIterator {
            words_on_screen: &self.word_manager.words_on_screen,
            index: 0,
        }
    }
}

pub struct TsGameVariablesIterator<'a> {
    words_on_screen: &'a [Box<crate::ts_word_on_screen::TsWordOnScreen>; 22],
    index: usize,
}

impl<'a> Iterator for TsGameVariablesIterator<'a> {
    type Item = &'a crate::ts_word_on_screen::TsWordOnScreen;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.index == self.words_on_screen.len()
            { return None; }
            let r = &self.words_on_screen[self.index];
            self.index += 1;
            if !r.word.is_empty() {
                return Some(r);
            }
        }
    }
}

impl TsGameVariables {
    pub(crate) fn new(ts_connection: Option<TsConnection>, wordlist: Vec<String>) -> Self {
        Self {
            stats: Default::default(),
            word_manager: TsWordManager::new(ts_connection, wordlist),
            rate: 1.0,
            start_time: SystemTime::now(),
            tick_time: SystemTime::now(),
        }
    }

    pub(crate) fn update_round(&mut self) {
        self.word_manager.add_word(self.stats.score);

        let rules = RULES.read().unwrap();
        self.rate = (self.stats.score / rules.step + rules.min_speed) as f64;

        if rules.max_speed > 0 && self.rate > rules.max_speed as f64 {
            self.rate = rules.max_speed as f64;
        }

        self.stats.update_speed();

        self.word_manager.add_network_word();
    }

    pub(crate) fn is_game_over(&self) -> bool {
        self.stats.misses >= RULES.read().unwrap().misses
    }

    pub(crate) fn advance_words(&mut self) -> Vec<((u16, u16), usize)> {
        let mut positions = vec![];

        for index in 0..self.word_manager.words_on_screen.len() {
            let word = &mut self.word_manager.words_on_screen[index];
            if word.length == 0 { continue; }

            if word.length + word.pos.0 == 79 {
                positions.push((word.pos, word.length as usize));
                self.stats.increase_misses();
                self.word_manager.remove_word(index);
            } else {
                word.advance_position();
            }
        }

        positions
    }

    pub(crate) fn remove_potential_hit(&mut self, entered_word: &String) -> Option<((u16, u16), usize)> {
        if let Some(word_metrics) = self.word_manager.remove_potential_hit(entered_word) {
            self.stats.increase_hits(word_metrics.1 as u16);
            return Some(word_metrics);
        }
        None
    }

    pub(crate) fn tick_not_over(&self) -> bool {
        ((self.tick_time.elapsed().unwrap().as_millis() / 10) as f64) < self.one_hundredth_rate()
    }

    pub(crate) fn update_time(&mut self) {
        self.stats.duration = (self.start_time.elapsed().unwrap().as_millis() / 10) as u64;
        self.tick_time = SystemTime::now();
    }

    fn one_hundredth_rate(&self) -> f64 {
        return 100.0f64 / self.rate;
    }
}
