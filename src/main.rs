mod ts_helpers;
mod ts_main;
mod ts_options;
mod ts_rules;
mod ts_stats;
mod ts_story;
mod ts_network;
mod ts_file;
mod ts_play;
mod ts_game_variables;
mod ts_word_on_screen;
mod ts_word_manager;
mod ts_connection;
mod ts_game_over;

use std::sync::RwLock;
use lazy_static::lazy_static;
use crate::ts_helpers::*;
use crate::ts_main::*;
use crate::ts_options::TsOptions;
use crate::ts_network::TsNetwork;
use crate::ts_rules::TsRules;

const TS_VERSION: &str = "v0.6.5";

// maximal number of lines to print words on
pub const MAX_LINES: usize = 22;

const INPUT_MAX: usize = 19;

lazy_static! {
    static ref RULES: RwLock<TsRules> = RwLock::new(TsRules::default());
    static ref OPTIONS: RwLock<TsOptions> = RwLock::new(TsOptions::default());
    static ref NETWORK: RwLock<TsNetwork> = RwLock::new(TsNetwork::default());
}


fn main() {
    create_terminal().expect("Not a proper terminal.");

    ctrlc::set_handler(move || {
        shut_down();
    }).expect("Error setting Ctrl-C handler");

    start();

    destroy_terminal();
}
