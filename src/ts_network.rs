use std::io::Stdout;
use std::net::{SocketAddr, TcpListener, TcpStream, ToSocketAddrs};
use std::time::Duration;
use crossterm::{event, execute, queue};
use crossterm::event::{Event, KeyCode, poll};
use crossterm::cursor::{MoveTo, MoveLeft};
use crossterm::style::{SetForegroundColor, Print};
use crate::{get_cleared_screen, get_input, NETWORK, OPTIONS, TS_VERSION};
use crate::ts_connection::TsConnection;
use crate::ts_options::{get_color, TSColors};

pub(crate) struct TsNetwork {
    port: u16,
    player_name: String,
    client: String,
}

impl TsNetwork {
    fn to_socket_address_string(&self) -> String {
        let address = self.client.to_owned() + ":" + self.port.to_string().as_ref();
        address.to_socket_addrs().unwrap().next().unwrap().to_string()
    }
}


impl Default for TsNetwork {
    fn default() -> Self {
        Self {
            port: 6025,
            player_name: String::from("default"),
            client: String::from(""),
        }
    }
}

pub(crate) fn network() -> Option<TsConnection> {
    loop {
        network_screen();

        if let Event::Key(key) = event::read().unwrap() {
            match key.code {
                KeyCode::Char('1') => return network_server(),
                KeyCode::Char('2') => {
                    let client = get_input("Enter Host: ".to_string(), 20, (30, 11));
                    NETWORK.write().unwrap().client = client;
                    return network_client();
                }
                KeyCode::Char('3') => match get_input("Enter Port: ".to_string(), 5, (30, 11)).parse::<u16>() {
                    Ok(x) => { NETWORK.write().unwrap().port = x; }
                    _ => ()
                },
                KeyCode::Char('4') => {
                    let player_name = get_input("Enter Name: ".to_string(), 20, (30, 11));
                    NETWORK.write().unwrap().player_name = player_name;
                }
                KeyCode::Char('5') => break,
                _ => (),
            }
        }
    }
    return None;
}

fn preamble() -> Stdout {
    let mut stdout = get_cleared_screen();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));

    queue!(stdout, reset_fg_color,
MoveTo(30, 3), Print("Typespeed ".to_owned() + TS_VERSION),
MoveTo(30, 5), Print("When you get connect, choose a word list and"),
MoveTo(30, 6), Print("then wait for the game start. The game starts as"),
MoveTo(30, 7), Print("soon as the other player has chosen a word list."),
        ).unwrap();

    stdout
}

fn network_client() -> Option<TsConnection> {
    let mut stdout = preamble();
    let network = NETWORK.read().unwrap();

    execute!(stdout,
            MoveTo(30, 10), Print("Connecting to typespeed server at"),
            MoveTo(30, 11), Print(format!("{} (port {})...",  network.to_socket_address_string(),  network.port )),
    ).unwrap();

    let addr = SocketAddr::from((std::net::Ipv4Addr::UNSPECIFIED, network.port));
    if let Ok(stream) = TcpStream::connect(addr) {
        let mut ts_connection = TsConnection::new(stream);

        let ts_id = " VERSION H2H ".to_owned() + TS_VERSION + "\n";
        ts_connection.write(&ts_id);

        ts_connection.set_blocking();
        let line = ts_connection.read().unwrap();

        eprintln!("connected: {}", line);
        ts_connection.set_nonblocking();

        return Some(ts_connection);
    }

    execute!(stdout,
            MoveTo(30, 13), Print("Cannot connect"),
            SetForegroundColor(get_color(TSColors::Auxiliary)),
            MoveTo(30, 15), Print("Press any key to continue..."),
            SetForegroundColor(get_color(TSColors::Cursor)),
            Print(" "), MoveLeft(1),
    ).unwrap();

    if let Event::Key(_) = event::read().unwrap() {
        return None;
    }
    None
}


fn network_server() -> Option<TsConnection> {
    let mut stdout = preamble();

    execute!(stdout,
            MoveTo(30, 10), Print("Waiting for connections..."),
            SetForegroundColor(get_color(TSColors::Cursor)),
            Print("  "), MoveLeft(1),
    ).unwrap();

    let addr = SocketAddr::from((std::net::Ipv4Addr::UNSPECIFIED, NETWORK.read().unwrap().port));
    let listener = TcpListener::bind(addr).unwrap();
    // Since we poll, blocking would be a show stopper
    listener.set_nonblocking(true).unwrap();
    loop {
        // check if we have a connection
        if let Ok(stream) = listener.incoming().next().unwrap() {
            let mut ts_connection = TsConnection::new(stream);

            ts_connection.set_blocking();

            let line = ts_connection.read().unwrap();
            eprintln!("{:?}", line);

            let ts_id = " VERSION H2H ".to_owned() + TS_VERSION + "\n";
            ts_connection.write(&ts_id);

            ts_connection.set_nonblocking();

            OPTIONS.write().unwrap().multiplayer = true;

            return Some(ts_connection);
        };

        // abort if a key was pressed
        if poll(Duration::from_millis(200)).unwrap() {
            if let Event::Key(_) = event::read().unwrap() {
                return None;
            }
        }
    }
}

pub(crate) fn network_screen() {
    let mut stdout = get_cleared_screen();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::DefaultText));
    let ts_network = NETWORK.read().unwrap();
    use crate::INPUT_MAX;

    execute!(stdout, reset_fg_color,
MoveTo(30, 3), Print("Typespeed ".to_owned() + TS_VERSION),
MoveTo(30, 5), Print("1. Server"),
MoveTo(30, 6), Print("2. Client"),
MoveTo(30, 7), Print("3. Port             "),
            Print(format!("{:<INPUT_MAX$}", ts_network.port.to_string())),
MoveTo(30, 8), Print("4. Player Name      "),
            Print(format!("{:<INPUT_MAX$}", ts_network.player_name)),
MoveTo(30, 9), Print("5. Return"),
MoveTo(30, 11), Print("Choose:"),
            SetForegroundColor(get_color(TSColors::Cursor)),
            Print("  "), MoveLeft(1),
).unwrap();
}


pub(crate) fn wait_for_party(ts_connection: &mut Option<TsConnection>) {
    if let None = ts_connection { return; }

    let mut stdout = get_cleared_screen();
    execute!(stdout,
        SetForegroundColor(get_color(TSColors::Cursor)),
        MoveTo(20, 12), Print("Waiting for the other party to join in... ")
    ).unwrap();

    ts_connection.as_mut().unwrap().wait_opponent_ready();
}