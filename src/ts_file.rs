use std::fs;
use std::fs::{File};
use std::io::{BufRead, BufReader, Read, Write};
use crossterm::{event, execute, queue};
use crossterm::style::{SetForegroundColor, Print};
use crossterm::cursor::{MoveTo, MoveLeft};
use crossterm::event::{Event, KeyCode};
use regex::Regex;
use crate::{footer, get_cleared_screen};
use crate::ts_options::{get_color, TSColors};
use std::env;

struct WordList
{
    file_name: String,
    name: String,
}
const TS_PATH_TO_WORDS: &str = "/words/";

pub(crate) fn get_wordlist() -> Vec<String> {
    let base_path = env::var("SNAP").expect("environment variable SNAP must point to words folder's parent.");
    let word_folder = base_path.to_owned() + &*TS_PATH_TO_WORDS.to_owned();
    let paths = fs::read_dir(word_folder.clone()).expect(&*format!("Could not open {}", word_folder));
    let word_files = Regex::new(r"words\..+").unwrap();

    let mut list_of_files = Vec::new();

    for e in paths {
        let dir_entry = e.unwrap();
        if word_files.is_match(dir_entry.file_name().to_str().unwrap()) {
            let file_name = dir_entry.file_name().to_str().unwrap().to_string();
            let file_path = word_folder.clone() + file_name.as_str();
            let file = File::open(file_path).unwrap();
            let name = BufReader::new(file).lines().next().unwrap().unwrap();
            list_of_files.push(WordList { file_name, name });
        }
    }

    let index = choose_file_screen(&list_of_files);
    let chosen_file = &list_of_files.get(index).unwrap().file_name;

    let mut wordlist = String::new();
     File::open(word_folder.clone() + chosen_file).unwrap().read_to_string(&mut wordlist).expect("I can only read utf-8 encoding.");

    let split = wordlist.split("\n");
    let words: Vec<String> = split.into_iter().map(|x| x.to_owned()).collect();

    words
}

fn choose_file_screen(list_of_files: &Vec<WordList>) -> usize {
    let mut stdout = get_cleared_screen();
    footer();

    let reset_fg_color = SetForegroundColor(get_color(TSColors::Auxiliary));
    let fg_color_cursor = SetForegroundColor(get_color(TSColors::Cursor));

    let mut marker: (u16, u16) = (1, 4);

    queue!(stdout, reset_fg_color,
        MoveTo(5, 2), Print("Choose a word list (UP/DOWN/ENTER):"),
    ).unwrap();


    for (i, e) in list_of_files.iter().enumerate() {
        queue!(stdout,
            MoveTo(5, 4 + i as u16), Print(e.name.clone()),
        ).unwrap();
    }

    stdout.flush().unwrap();

    loop {
        execute!(stdout, reset_fg_color,
            MoveTo(marker.0, marker.1), Print("->"), fg_color_cursor, Print(" "), MoveLeft(1)
        ).unwrap();

        if let Event::Key(key) = event::read().unwrap() {
            match key.code {
                KeyCode::Up => {
                    if marker.1 != 4 {
                        queue!(stdout, MoveTo(marker.0, marker.1), Print("  ") ).unwrap();
                        marker.1 -= 1;
                    }
                }
                KeyCode::Down => {
                    if marker.1 != 3 + list_of_files.len() as u16 {
                        queue!(stdout, MoveTo(marker.0, marker.1), Print("  ") ).unwrap();
                        marker.1 += 1;
                    }
                }
                KeyCode::Enter => return (marker.1 - 4) as usize,
                _ => continue,
            }
        }
    }
}
