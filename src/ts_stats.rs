use crossterm::style::*;
use crossterm::cursor::MoveTo;
use crossterm::queue;
use crate::ts_options::{get_color, TSColors};
use std::io::stdout;

pub struct TsStats {
    pub(crate) score: u16,
    pub(crate) misses: u8,
    pub(crate) duration: u64,
    pub(crate) rank: String,
    pub(crate) wpm: u8,
    pub(crate) speed: f64,
    words_written: u16,
}

impl Default for TsStats {
    fn default() -> Self {
        Self {
            score: 0,
            misses: 0,
            duration: 0,
            rank: "None".to_string(),
            wpm: 0,
            speed: 0.0,
            words_written: 0,
        }
    }
}

pub fn stats_legends() {
    let mut stdout = stdout();

    queue!(stdout, MoveTo(0, 23), Print(" >                   <".with(get_color(TSColors::Auxiliary)))).unwrap();
    queue!(stdout, MoveTo(23, 23), Print("Rank:          Score:    0 WPM:   0 CPS:  0.00 Misses:  0".with(get_color(TSColors::DefaultText)))).unwrap();
}

pub fn stats_metrics(stats: &TsStats) {
    let fg_color_rank = SetForegroundColor(get_color(TSColors::Rank));

    queue!(stdout(),
        fg_color_rank,
        MoveTo(29, 23), Print(format!("{:<9}", stats.rank)),
        SetForegroundColor(get_color(TSColors::Metric)),
        MoveTo(45, 23), Print(format!("{:>4}", stats.score)),
        MoveTo(55, 23), Print(format!("{:>3}", stats.wpm)),
        MoveTo(65, 23), Print(format!("{:0>2.2}", stats.speed)),
        MoveTo(78, 23), Print(format!("{:>2}", stats.misses)),
    ).unwrap();
}

impl TsStats {
    pub(crate) fn increase_misses(&mut self) {
        self.misses += 1;
    }

    pub(crate) fn increase_hits(&mut self, score: u16) {
        self.score += score;
        self.words_written += 1;
    }

    pub(crate) fn update_speed(&mut self) {
        self.speed = (self.score + self.words_written) as f64 * 100.0 / self.duration as f64;
        self.wpm = (self.speed * 12.0) as u8; //12? really?
    }
}
