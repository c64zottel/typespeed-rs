# Typespeed-rs #
typespeed-rs is a rewrite in Rust of [typespeed](https://gitlab.com/c64zottel/typespeed-original), a game where you have to type as fast as you can:

![](in_game.png)

My objectives with this project:
- Learn Rust
- Rewrite typespeed as close as possible, including network communication and all quirks.
